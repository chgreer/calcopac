#!/usr/bin/env python
import numpy as np
import math
import atmosphere as asph

def opacity(tamb,relh,el=90,freq=230,alt0=3.186):
    """
    computes the value of the opacity

    inputs: tamb - outdoor temp (K)
            relh - relative humidity (%)
            el=90 - elevation (deg)
            freq=230 - frequency (GHz)
            alt0=3.186 - site altitude (km)

    output: tau - opacity at elevation el 
    """
    tau0 = zen_opacity(tamb,relh,freq,alt0)
    amass = air_mass(el)
    tau = amass*tau0
    return tau


def zen_opacity(tamb,relh,freq=230,alt0=3.186):

    #h2o vap press (mmHg) from Claussus Clapeyron model
    #plambeck's expression
    #h2ovappress = 1.598e9 * exp(-5.37e3/tamb)
    h2o_vap_press = 3.78e8 * np.exp(-4895.0/tamb)

    #partial pressure of water (mmHg) at current humidity
    h2o_part_press = relh/100.0 * h2o_vap_press

    #ground level water vapor density in grams/cubic meter 
    #reference:  Allen, Astrophysical Quantities, 1973
    ground_hum = h2o_part_press/tamb*288.6

    #start at the base altitude alt0, and add up opacity by layer
    alt = alt0
    tau = 0.0
    dtau = 0.0
    dalt = 0.4 #km

    while alt < 20.0:
        #  returns the atmospheric static at midpoint of layer
        atm = asph.Atmosphere(alt + dalt/2., ground_hum, alt0);

        #zenith opacity of that layer
        dtau = o2_absorb(freq,atm.pressure,atm.temperature) + \
            h2o_absorb(freq,atm.pressure,atm.temperature,atm.density) 
        dtau = dtau * 1e5 * dalt
        tau += dtau
        alt += dalt
        
        if alt>5:
            #just to speed up at higher alt
            dalt = 1.0 #km

    return tau

def o2_absorb(nu,p,temp):
    """
    function O2Absorption

    calculates the oxygen absorption coefficient (cm-1) at a given frequency
    
    inputs: frequency   (nu)   [GHz]   
            pressure    (p)    [millibars]
            temperature (temp) [Kelvin]

    output: Oxygen absorption coefficient [cm-1]
    """
	   
    #frequency of transitions as given by table II (page 160) 
    del_nu_b = 0.0527;
    nu_n_minus = np.array([118.750343,62.486255,60.306044,59.164215,58.323885,\
        57.612488,56.988180,56.363393,55.783819,55.221372,54.671145,54.1302,\
        53.5959,53.0669,52.5424,52.0214,51.50302,50.9873,50.4736,49.9618])
    nu_n_plus = np.array([56.264766,58.446580,59.590978,60.434776,61.150570,\
        61.800169,62.411223,62.997991,63.568520,64.127777,64.678914,65.22412,\
        65.764744,66.30206,66.83677,67.36951,67.90073,68.4308,68.9601,69.4887])

    #empirical bandwidth calculation (equation 2.3.29)
    del_nu1 = 1.41e-3 * p * 300.0/temp
    del_nu2 = 2.0/3 * del_nu_b + del_nu1/3.0;
    if del_nu1 <= del_nu_b:
        del_nu = del_nu1
    else:
        del_nu = del_nu2

    #now to actually calculate the sum (according to equation 2.3.30)
    sum_term = 0.0
    for ii in np.arange(20):
        N = 2*ii+1;

        #sum is gn+ + gn- + gn0 
        if N==1:
            del_nu_excep = del_nu1
        else:
            del_nu_excep = del_nu

        termPlus  = N*(2*N+3)/(N+1) * nu_n_plus[ii] * \
            f_kinetic(nu, nu_n_plus[ii], del_nu);
        termMinus = (N+1)*(2*N - 1)/(N) * nu_n_minus[ii] * \
            f_kinetic(nu, nu_n_minus[ii], del_nu_excep);
        termZero  = (math.pow(N,2) + N + 1)*(2.0*N + 1)/(N * (N + 1)) * \
            (2.0 * nu * del_nu1) / \
            (math.pi * (math.pow(nu,2) + math.pow(del_nu1,2))); 
        factor = math.exp(-2.07 * N * (N + 1) / temp);

        sum_term += ( termPlus + termMinus + termZero ) * factor;
  
    KOxygen = 1.44e-5 * p * pow(temp,(-3)) * nu * sum_term;

    return KOxygen;

def h2o_absorb(nu,p,temp,rho):
    """
    function H2OAbsorption

    Calculates the water absorption coefficient (cm-1) at a given 
    frequency nu.

    inputs: observing frequency (nu)    [GHz]
            pressure            (p)     [GHz]
            temperature         (temp)  [Kelvin]
            water vapor density (rho)   [g/cubic meter]

    output: water absorption coefficient [cm-1]
    """

    #declarations - all from Table I on page 153 */
    #transition frequencies
    nu_lm = np.array([22.23515,183.31012,323.0,325.1538,380.1968,390.0,436.0,\
        438.0,442.0,448.0008])  
    #gaunt spin factor
    gl = np.array([3.0,1.0,3.0,1.0,3.0,1.0,1.0,3.0,3.0,3.0])
    phi_squ = np.array([0.0549,0.1015,0.0870,0.0891,0.1224,0.0680,0.0820,\
        0.0987,0.0820,0.1316]);
    #next two in cm^-1
    em = np.array([447.30,142.27,1293.80,326.62,224.84,1538.31,1059.63,756.76,\
        1059.90,300.37])  
    el = np.array([446.56,136.16,1283.02,315.78,212.16,1525.31,1045.03,742.11,\
        1045.11,285.42])
    #next two in GHz
    del_zero = np.array([2.85,2.68,2.30,3.03,3.19,2.11,1.50,1.94,1.51,2.47])
    del_water = np.array([13.68,14.49,12.04,15.21,15.84,11.42,7.94,10.44,\
        8.13,14.24])
    x = np.array([0.626,0.649,0.420,0.619,0.630,0.330,0.290,0.360,0.332,0.510])
    
    #kT/hc  - necessary to convert el, em
    freqX = 0.69457 * temp
    sum_term = 0.0
    for ii in np.arange(10):
        #linewidth parameter calculation
        del_nu_lm = del_zero[ii]*(p/1013.0) * math.pow((temp/300.0),-x[ii]) *\
            (1.0 + 4.6e-3*rho*temp/p * ((del_water[ii]/del_zero[ii]) - 1))

        sum_term += ( math.exp(-el[ii]/freqX) - math.exp(-em[ii]/freqX) ) * \
            gl[ii] * phi_squ[ii] * f_kinetic(nu, nu_lm[ii], del_nu_lm)

    KWater = 1.44 * rho * nu * math.pow((temp),(-1.5)) * sum_term;
    KWater += 1.08e-11 * rho * math.pow((300.0/temp),2.1) * \
        (p/1000.) * math.pow(nu,2);

    return KWater;


def f_kinetic(nu,nu_lm,del_nu):
    """
    function fKinetic
    
    Determines the kinetic line shape of a transition (equation 2.3.12)
    
    inputs: observing frequency (nu) [GHz]
            transition frequency (nuLM) [GHz]
            linewidth parameter (deltaNu) [GHz]
    
    output:  width 
    """
    if del_nu > 1e18:
        res = 0.0
    else:
        res_den = math.pow((math.pow(nu_lm,2) - math.pow(nu,2)),2)  + \
            4*math.pow(nu,2)*math.pow(nu_lm,2)
        res = 1.27324 * nu * nu_lm * del_nu / res_den

    return res

def air_mass(elev):
    """
    function: air_mass(elev)
    
    calculates the air mass for a given elevation (in degrees)
    """
    elev *= math.pi/180;
    amass = 1/np.sin(elev)
    return amass


