#!/usr/bin/env python
import numpy as np
import math

class Atmosphere:
    def __init__(self,elev,rel_hum,gnd_elev=3.186):
        """
        class: atmosphere
    
        calculates the state of the atmosphere based on the 1976 U.S. Standard
        Atmosphere.  That is, returns temp, press, and water vapor density 
        given altitude, ground level humidity, and ground level altitude;

        source:  http://www.digitaldutch.com/atmoscalc/
    
        in:     elevation             (elev)      [km]
                ground level humidity (rel_hum)  [g/m^3]
                ground elevation      (gnd_elev) [km]

        out:    self.temperature [K]
                self.pressure    [millibars]
                self.density (water vapor) [g/m^3]
        """
        #assumed to be 2.0km between 1.5 and 2
        h2o_scale_height = 2.0;  

        #Kelvin/100
        atm_temp = np.array([2.8815, 2.8165, 2.7515, 2.6865, 2.6215, 2.5565,\
            2.4915,2.4265,2.3615,2.2965,2.2315,2.1665,2.1665,2.1665,2.1665, \
            2.1665,2.1665,2.1665,2.1665,2.1665,2.1665,2.1765,2.1865,2.1965,\
            2.2065,2.2165,2.2265,2.2365,2.2465,2.2565,2.2665,2.2765,2.2865,\
            2.3145,2.3425,2.3705,2.3985,2.4265,2.4545,2.4825,2.5105,2.5385,\
            2.5665,2.5945,2.6225,2.6505,2.6785,2.7065,2.7065,2.7065,2.7065,\
            2.7065,2.6785,2.6505,2.6225,2.5945,2.5665,2.5385,2.5105,2.4825,\
            2.4545,2.4265,2.3985,2.3705,2.3425,2.3145,2.2865,2.2585,2.2305,\
            2.2025, 2.1745])   
    
        #millibars/1000
        atm_pres = np.array([1.01325,0.8987457,0.7949522,0.7010854,0.6164024,\
            0.5401991,0.4718103,0.4106074,0.3559981,0.3074246,0.2643627,\
            0.2263206,0.1933041,0.1651041,0.1410180,0.1204457,0.1028746,\
            0.0878668,0.0750484,0.0641001,0.0547489,0.0467789,0.0399979,\
            0.0342243,0.0293049,0.0251102,0.0215309,0.0184746,0.0158629,\
            0.0136296,0.0117187,0.0100823,0.0086802,0.0074823,0.0064612,\
            0.0055892,0.0048432,0.0042037,0.0036545,0.0031822,0.0027752,\
            0.0024240,0.0021203,0.0018574,0.0016294,0.0014313,0.0012591,\
            0.0011091,0.0009775,0.0008616,0.0007594,0.0006694,0.0005896,\
            0.0005187,0.0004556,0.0003997,0.0003501,0.0003063,0.0002675,\
            0.0002333,0.0002031,0.0001766,0.0001533,0.0001328,0.0001149,\
            0.0000992,0.0000855,0.0000736,0.0000632,0.0000542,0.0000463])

        h = np.arange(len(atm_temp)) #in km
        atm_temp *= 100 #in K
        atm_pres *= 1000 #in millibars

        ii=0
        while ((ii < len(h)-1) and (elev > h[ii])):
            ii += 1
            fac = (elev - h[ii-1]) / (h[ii] - h[ii-1])
            temp = atm_temp[ii-1] + fac * (atm_temp[ii] - atm_temp[ii-1])
            pres = atm_pres[ii-1] + math.pow(atm_pres[ii]/atm_pres[ii-1],fac)
            if elev<15:
                dens = rel_hum * np.exp( (gnd_elev-elev)/h2o_scale_height)
            else:
                dens = 4.7569e-4 * pres/temp
            
        self.temperature = temp
        self.pressure = pres
        self.density = dens
