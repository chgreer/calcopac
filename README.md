calcopac
=======

Code to return the value of the GHz opacity given weather information and 
observation elevation.

Includes O2 lines from ~45-120 GHz.
Includes H2O lines from ~20-450 GHz.

Example code:

	import calcopac as co

    freq = 230 #(GHz)
    tamb = 276.600 #K
	relH = 85.5 
	alt = 1.22 #km	
	el = 45 #telescope elevation deg
	tau0 = co.opacity(tamb,relH,el=90,freq=230,alt0=1.22)
	print tau0
	>>>0.772235150345
